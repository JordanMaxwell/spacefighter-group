﻿
/*
     ██╗  ██╗  █████╗  ████████╗  █████╗  ███╗   ██╗  █████╗ 
	 ██║ ██╔╝ ██╔══██╗ ╚══██╔══╝ ██╔══██╗ ████╗  ██║ ██╔══██╗
	 █████╔╝  ███████║    ██║    ███████║ ██╔██╗ ██║ ███████║
	 ██╔═██╗  ██╔══██║    ██║    ██╔══██║ ██║╚██╗██║ ██╔══██║
	 ██║  ██╗ ██║  ██║    ██║    ██║  ██║ ██║ ╚████║ ██║  ██║
	 ╚═╝  ╚═╝ ╚═╝  ╚═╝/\  ╚═╝    ╚═╝  ╚═╝ ╚═╝  ╚═══╝ ╚═╝  ╚═╝
   /vvvvvvvvvvvvvvvvvvv \=========================================,
   `^^^^^^^^^^^^^^^^^^^ /---------------------------------------"
        Katana Engine \/ © 2012 - Shuriken Studios LLC
			    http://www.shurikenstudios.com
*/

#include "KatanaEngine.h"

namespace KatanaEngine
{
	MenuItem::MenuItem() : MenuItem("")	{ }

	MenuItem::MenuItem(std::string text)
	{
		m_onSelect = nullptr;
		m_pFont = nullptr;

		m_text = text;

		m_color = Color::White;
		m_alpha = 1.0f;

		m_position = Vector2::ZERO;
		m_textOffset = Vector2::ZERO;

		m_textAlign = TextAlign::LEFT;
		m_isDisplayed = true;

		m_selectSample = nullptr;
	}

	void MenuItem::Draw(SpriteBatch *pSpriteBatch)
	{
		if (m_pFont && m_text.compare("") != 0)
		{
			pSpriteBatch->DrawString(m_pFont, &m_text, m_position + m_textOffset, m_color * m_alpha, m_textAlign);
		}
	}

	void MenuItem::Select(MenuScreen* pMenuScreen)
	{
		if (m_onSelect) ((OnSelect)m_onSelect)(pMenuScreen);
	}

	void MenuItem::SetSelected(const bool isSelected) {

		// Change selected state
		if (isSelected == m_isSelected) return;
		m_isSelected = isSelected;

		// Play registered audio instance if any
		if (m_selectSample != nullptr && m_isSelected) {
			al_play_sample_instance(m_selectSample);
		}
	}

	void MenuItem::SetSelectAudio(ALLEGRO_SAMPLE* selectAudio) {
		m_selectSample = al_create_sample_instance(selectAudio);
		al_attach_sample_instance_to_mixer(m_selectSample, al_get_default_mixer());
		al_set_sample_instance_playmode(m_selectSample, ALLEGRO_PLAYMODE_ONCE);
	}
}