
#include "Asteroid.h"


Asteroid::Asteroid()
{
	SetSpeed(100);
	SetMaxHitPoints(4);
	SetCollisionRadius(20);
}


void Asteroid::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		// Move asteroid down screen
		TranslatePosition(GetPosition().X, GetSpeed() * pGameTime->GetTimeElapsed());

		// Spin Asteroid
		m_rotation += 0.05;
		if (m_rotation > 360)
			m_rotation = 0;

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void Asteroid::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, m_rotation, 1);
	}
}
