
#pragma once

#include "EnemyShip.h"

class Asteroid : public EnemyShip
{

public:

	Asteroid();
	virtual ~Asteroid() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;
	float m_rotation = 0;
};