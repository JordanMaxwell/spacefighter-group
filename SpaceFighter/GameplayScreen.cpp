
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"

GameplayScreen::GameplayScreen(const int levelIndex)
{
	m_pLevel = nullptr;
	switch (levelIndex)
	{
		case 0: m_pLevel = new Level01(); break;
	}

	m_pBackground = nullptr;

	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pGameplay = al_load_sample("Content\\Audio\\menu.wav");
	m_pGameplayInstance = al_create_sample_instance(m_pGameplay);
	al_set_sample_instance_playmode(m_pGameplayInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(m_pGameplayInstance, al_get_default_mixer());
	al_play_sample_instance(m_pGameplayInstance);

	m_pBackground = pResourceManager->Load<Texture>("Textures\\Backgrounds\\black.png");
	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	// Draw scrolling space background
	int drawWidth = Game::GetScreenWidth() / m_pBackground->GetWidth();
	int drawHeight = (Game::GetScreenHeight() / m_pBackground->GetHeight()) * 2;
	for (int x = 0; x <= drawWidth; x++) {
		for (int y = -Game::GetScreenHeight(); y <= drawHeight; y++) {
			Vector2 drawPos = Vector2(
				x * m_pBackground->GetWidth(),
				y * m_pBackground->GetHeight() + m_backgroundOffset);
			pSpriteBatch->Draw(m_pBackground, drawPos, Color::White * GetAlpha(), m_pBackground->GetCenter());
		}
	}

	// Draw level ontop of background and end batch draw
	m_pLevel->Draw(pSpriteBatch);
	pSpriteBatch->End();

	// Change background offset
	m_backgroundOffset += 30;
	if (m_backgroundOffset >= Game::GetScreenHeight()) {
		m_backgroundOffset = 0;
	}
}
