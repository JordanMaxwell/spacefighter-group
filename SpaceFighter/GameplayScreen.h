
#pragma once

#include "KatanaEngine.h"

using namespace KatanaEngine;

class Level;

class GameplayScreen : public Screen
{

public:

	GameplayScreen(const int levelIndex = 0);

	virtual ~GameplayScreen() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void HandleInput(const InputState *pInput);

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);


private:

	Level *m_pLevel;

	Texture* m_pBackground;

	float m_backgroundOffset = 0;
	Vector2 m_texturePosition;

	ALLEGRO_SAMPLE *m_pGameplay;
	ALLEGRO_SAMPLE_INSTANCE *m_pGameplayInstance;
};
