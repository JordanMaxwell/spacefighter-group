

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pBlackLevel1Texture = pResourceManager->Load<Texture>("Textures\\Enemies\\enemyBlack1.png");
	Texture* pBlueLevel1Texture = pResourceManager->Load<Texture>("Textures\\Enemies\\enemyBlue1.png");
	Texture* pGreenLevel1Texture = pResourceManager->Load<Texture>("Textures\\Enemies\\enemyGreen1.png");
	Texture* pRedLevel1Texture = pResourceManager->Load<Texture>("Textures\\Enemies\\enemyRed1.png");

	int maxLevel = 5;
	std::string colors[4]{ "Black", "Blue", "Green", "Red" };

	const int COUNT = 21;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 1.0; // start delay
	Vector2 position;
	Texture* pTexture = nullptr;

	for (int i = 0; i < COUNT; i++)
	{
		// Pick a random color and level
		int color_index = (rand() % 4);
		int level_index = (rand() % maxLevel) + 1;
		std::stringstream filename;
		filename << "Textures\\Enemies\\enemy" << colors[color_index] << level_index << ".png";
		pTexture = pResourceManager->Load<Texture>(filename.str());

		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->LoadContent(pResourceManager);
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->SetMaxHitPoints(level_index);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

