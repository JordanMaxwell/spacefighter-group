
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"


// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	//sets flag for when the gameplay starts
	MainMenuScreen* pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetGameTag();

	//stops the menu music when the gameplay starts
	if (pMainMenuScreen->IsInGame()) pMainMenuScreen->StopAudio();
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}

MainMenuScreen::MainMenuScreen()
{
	m_pLogo = nullptr;
	m_pBackground = nullptr;

	SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(2.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}



void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pLogo = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 220;

	// Background
	m_pBackground = pResourceManager->Load<Texture>("Textures\\Backgrounds\\black.png");

	// Audio
	//initializes allegro audio
	al_init();
	al_install_audio();
	al_init_acodec_addon();

	//reserves space for samples and loads sample sounds
	m_pMainMenu = al_load_sample("Content\\Audio\\menu.wav");

	//creates an instance to link to a mixer in order to have more control over the menu music
	m_pSongInstance = al_create_sample_instance(m_pMainMenu);
	al_set_sample_instance_playmode(m_pSongInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(m_pSongInstance, al_get_default_mixer());

	//plays menu music
	al_play_sample_instance(m_pSongInstance);

	// Create the menu items
	const int COUNT = 2;
	MenuItem *pItem;
	Font::SetLoadSize(20, true);
	Font *pFont = pResourceManager->Load<Font>("Fonts\\future.ttf");

	SetDisplayCount(COUNT);

	enum Items { START_GAME, QUIT };
	std::string text[COUNT] = { "Start Game", "Quit" };

	m_pMenuSelect = al_load_sample("Content\\Audio\\lazer_long.wav");
	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Game::GetScreenCenter() + Vector2::UNIT_Y * (50 * i));
		pItem->SetFont(pFont);
		pItem->SetTextAlign(TextAlign::CENTER);
		pItem->SetColor(Color::White);
		pItem->SetSelected(i == 0);
		pItem->SetSelectAudio(m_pMenuSelect);
		AddMenuItem(pItem);
	}

	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect);
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect);
	
}


void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;
	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());


		if (pItem->IsSelected()) pItem->SetColor(Color::Blue);
		else pItem->SetColor(Color::White);
	}
	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	// Start sprite batch draw
	pSpriteBatch->Begin();

	// Draw scrolling space background
	int drawWidth = Game::GetScreenWidth() / m_pBackground->GetWidth();
	int drawHeight = (Game::GetScreenHeight() / m_pBackground->GetHeight()) * 2;
	for (int x = 0; x <= drawWidth; x++) {
		for (int y = -Game::GetScreenHeight(); y <= drawHeight; y++) {
			Vector2 drawPos = Vector2(
				x * m_pBackground->GetWidth(),
				y * m_pBackground->GetHeight() + m_backgroundOffset);
			pSpriteBatch->Draw(m_pBackground, drawPos, Color::White * GetAlpha(), m_pBackground->GetCenter());
		}
	}

	// Draw logo and end batch
	pSpriteBatch->Draw(m_pLogo, m_texturePosition, Color::White * GetAlpha(), m_pLogo->GetCenter());
	pSpriteBatch->End();

	// Change background offset
	m_backgroundOffset += 50;
	if (m_backgroundOffset >= Game::GetScreenHeight()) {
		m_backgroundOffset = 0;
	}

	// Draw Menu UI
	MenuScreen::Draw(pSpriteBatch);
}

void MainMenuScreen::StopAudio() {
	//stops the menu music from playing
	al_stop_sample_instance(m_pSongInstance);
}

