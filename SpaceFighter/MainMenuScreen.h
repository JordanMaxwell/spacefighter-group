#pragma once

#include "KatanaEngine.h"

using namespace KatanaEngine;

class MainMenuScreen : public MenuScreen
{

public:

	MainMenuScreen();

	virtual ~MainMenuScreen() { }

	virtual void LoadContent(ResourceManager *pResourceManager);
	
	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);


	virtual void SetQuitFlag() { m_isQuittingGame = true; }

	virtual bool IsQuittingGame() { return m_isQuittingGame; }

	virtual void StopAudio();

	virtual void SetGameTag() { m_inGame = true; }

	virtual bool IsInGame() { return m_inGame; }



private:
	
	Texture *m_pLogo;
	Texture* m_pBackground;

	float m_backgroundOffset = 0;
	Vector2 m_texturePosition;

	bool m_isQuittingGame = false;
	bool m_inGame = false;

	ALLEGRO_SAMPLE *m_pMainMenu = NULL;
	ALLEGRO_SAMPLE *m_pMenuSelect = NULL;
	ALLEGRO_SAMPLE_INSTANCE *m_pSongInstance = NULL;
	
};