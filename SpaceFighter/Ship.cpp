
#include "Ship.h"


Ship::Ship()
{
	SetPosition(0, 0);
	SetCollisionRadius(10);

	m_speed = 300;
	m_maxHitPoints = 3;
	m_isInvulnurable = false;

	//Audio Initialization
	m_pHitSound = al_load_sample("Content\\Audio\\\enemy_damage_1.wav");
	m_pDeathSound = al_load_sample("Content\\Audio\\explosion_1_large_2.wav");

	m_pDeathSoundInstance = al_create_sample_instance(m_pDeathSound);
	m_pHitSoundInstance = al_create_sample_instance(m_pHitSound);
	al_set_sample_instance_playmode(m_pHitSoundInstance, ALLEGRO_PLAYMODE_ONCE);
	al_set_sample_instance_playmode(m_pDeathSoundInstance, ALLEGRO_PLAYMODE_ONCE);

	al_attach_sample_instance_to_mixer(m_pDeathSoundInstance, al_get_default_mixer());
	al_attach_sample_instance_to_mixer(m_pHitSoundInstance, al_get_default_mixer());

	Initialize();
}

void Ship::LoadContent(ResourceManager* pResourceManager) {
	m_pExplosion = pResourceManager->Load<Texture>("Textures\\Explosion.png");
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	if (m_flagRemoval) {
		m_removalDelay -= pGameTime->GetTimeElapsed();
		if (m_removalDelay <= 0) {
			GameObject::Deactivate();
		}
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage)
{
	// Determine if the ship is invulnerable
	if (!m_isInvulnurable)
	{
		// Subtract the delt damage from our current hit point status
		m_hitPoints -= damage;

		// If our hit points are equal or less then zero then we have "died". 
		// This process deactivates the game object to remove it from render.
		if (m_hitPoints <= 0)
		{
			// Change sprite to an explosion and flag for removal at a later frame
			m_pTexture = m_pExplosion;
			al_play_sample_instance(m_pDeathSoundInstance);
			m_removalDelay = 2;
			m_flagRemoval = true;
		}
		else {
			// Not enough damage was taken to explode. Just play a damage sound instead
			al_play_sample_instance(m_pHitSoundInstance);
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

void Ship::FireWeapons(TriggerType type)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}