
#pragma once

#include "GameObject.h"
#include "Weapon.h"


class Ship : public GameObject
{
public:
	Ship();
	virtual ~Ship() { }

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void Update(const GameTime *pGameTime);

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }
	void SetExplosion(Texture* pTexture) { m_pExplosion = pTexture; }

	// EX: Pure Virtual
	virtual void Draw(SpriteBatch *pSpriteBatch) = 0;

	virtual void Hit(const float damage);

	virtual bool IsInvulnurable() const { return m_isInvulnurable; }

	virtual void SetInvulnurable(bool isInvulnurable = true) { m_isInvulnurable = isInvulnurable; }

	virtual std::string ToString() const { return "Ship"; }

	virtual CollisionType GetCollisionType() const = 0;

	virtual void AttachWeapon(Weapon *pWeapon, Vector2 position);

	virtual float GetSpeed() const { return m_speed; }

	virtual void SetSpeed(const float speed) { m_speed = speed; }

	virtual void SetMaxHitPoints(const float hitPoints) { m_maxHitPoints = hitPoints; }


protected:

	virtual void Initialize();

	virtual void FireWeapons(TriggerType type = TriggerType::ALL);

	virtual Weapon *GetWeapon(const int index) { if (index < m_weapons.size()) return m_weapons[index]; return nullptr; }

	virtual float GetHitPoints() const { return m_hitPoints; }

	virtual float GetMaxHitPoints() const { return m_maxHitPoints; }

	Texture* m_pTexture;

private:

	float m_speed;

	float m_hitPoints;
	float m_maxHitPoints;

	bool m_isInvulnurable;

	bool m_flagRemoval = false;
	int m_removalDelay = 0;

	std::vector<Weapon *> m_weapons;
	std::vector<Weapon *>::iterator m_weaponIt;

	Texture *m_pExplosion;

	ALLEGRO_SAMPLE *m_pHitSound;
	ALLEGRO_SAMPLE *m_pDeathSound;
	ALLEGRO_SAMPLE_INSTANCE *m_pHitSoundInstance;
	ALLEGRO_SAMPLE_INSTANCE *m_pDeathSoundInstance;
};

